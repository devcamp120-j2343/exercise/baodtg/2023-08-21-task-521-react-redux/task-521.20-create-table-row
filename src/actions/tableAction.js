import { INPUT_TASK_NAME, ADD_TASK_CLICKED, INPUT_FILTER_TASK_NAME, FILTER_TASK_CLICKED } from "../constants/tableConstants";

export const inputTaskNameAction = (inputValue) => {
    return {
        type: INPUT_TASK_NAME,
        payload: inputValue
    }
}

export const addTaskAction = () => {
    return {
        type: ADD_TASK_CLICKED
    }
}

export const inputFilterTaskNameAction = (inputValue) => {
    return {
        type: INPUT_FILTER_TASK_NAME,
        payload: inputValue
    }
}
export const filterTaskNameAction = () => {
    return {
        type: FILTER_TASK_CLICKED,
    }
}