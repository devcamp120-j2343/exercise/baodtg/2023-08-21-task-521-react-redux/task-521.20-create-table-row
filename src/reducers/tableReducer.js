import { ADD_TASK_CLICKED, FILTER_TASK_CLICKED, INPUT_FILTER_TASK_NAME, INPUT_TASK_NAME } from "../constants/tableConstants";

// Định nghĩa state khởi tạo cho table
const initialState = {
    inputString: "",
    tableRow: [],
    filterString: "",
    tableFilter: []
}

//Định nghĩa khai báo table reducer:
const tableReducer = (state = initialState, action) => {
    //Update state
    switch (action.type) {
        case INPUT_TASK_NAME:
            state.inputString = action.payload
            break;
        case ADD_TASK_CLICKED:
            state.tableRow.push(state.inputString)
            break;
        case INPUT_FILTER_TASK_NAME:
            state.filterString = action.payload
            break;
        case FILTER_TASK_CLICKED:
            state.tableFilter = state.tableRow.filter((taskName) => taskName === state.filterString)
            break;
        default:
            break;
    }
    return { ...state }
}
export default tableReducer;