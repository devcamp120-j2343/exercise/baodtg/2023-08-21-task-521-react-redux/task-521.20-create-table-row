// Root reducer
import { combineReducers } from "redux";

// Khai báo các reducer con
import tableReducer from "./tableReducer";


// Tạo một root reducer 
const rootReducer = combineReducers({
    tableReducer
})
export default rootReducer