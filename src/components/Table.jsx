import 'bootstrap/dist/css/bootstrap.min.css'
import { useDispatch, useSelector } from 'react-redux'
import { Button, Col, Container, Row, Table } from 'reactstrap'
import { addTaskAction, filterTaskNameAction, inputFilterTaskNameAction, inputTaskNameAction } from '../actions/tableAction';

function AddRow() {
    // Khai báo hàm dispatch sự kiện tới redux store
    const dispatch = useDispatch();
    // useSelector để đọc state từ redux
    const { inputString, tableRow, filterString, tableFilter } = useSelector((reduxData) => {
        return reduxData.tableReducer
    })

    const inputChangeHandler = (event) => {
        dispatch(inputTaskNameAction(event.target.value));
    }

    const addTaskClickHandler = () => {
        console.log(inputString);
        dispatch(addTaskAction());
    }
    const inputFilterChangeHandler = (event) => {
        dispatch(inputFilterTaskNameAction(event.target.value));
    }
    const filterTaskClickHandler = () => {
        dispatch(filterTaskNameAction());
    }

    return (
        <>
            <Container className='text-center mt-5'>
                <Row>
                    <Col>
                        <input className='w-75' value={inputString} onChange={inputChangeHandler} />
                        <Button className='ms-1' onClick={addTaskClickHandler} >Thêm dòng</Button>

                    </Col>

                </Row>
                <Row>
                    <Col>
                        <input className='w-75 mt-2' value={filterString} onChange={inputFilterChangeHandler} />
                        <Button className='ms-1' onClick={filterTaskClickHandler}>Lọc task</Button>
                    </Col>
                </Row>
                <Table bordered striped hover className='mt-3 table-primary'>
                    <thead>
                        <tr>
                            <td>STT</td>
                            <td>Nội dung</td>
                        </tr>
                    </thead>
                    <tbody>

                        {
                            tableRow.map((element, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{element}</td>
                                    </tr>
                                )

                            })
                        }
                        
                    </tbody>
                </Table>

                <Table bordered striped hover className='mt-3 table-primary'>
                    <thead>
                        <tr>
                            <td>STT</td>
                            <td>Nội dung lọc</td>
                        </tr>
                    </thead>
                    <tbody>

                        {
                            tableFilter.map((element, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{element}</td>
                                    </tr>
                                )

                            })
                        }
                    </tbody>
                </Table>
            </Container>

        </>
    )
}
export default AddRow